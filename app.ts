import express from "express";
import session from "express-session";
import bodyParser from "body-parser";
import Knex from 'knex';
import configs from "./knexfile";
import { isLoggedIn } from "./guard";
import dotenv from 'dotenv';
dotenv.config();
import grant from 'grant-express';

const PORT = 8080;
const app = express();
const knex = Knex(configs.development);

app.use(session({
    secret: "secret-key",
    resave: true,
    saveUninitialized: true,
    cookie: { secure: false }
}));

app.use(grant({
    "defaults": {
        "protocol": "http",
        "host": process.env.HOST || "localhost:8080",
        "transport": "session",
        "state": true,
    },
    "google": {
        "key": process.env.GOOGLE_CLIENT_ID || "",
        "secret": process.env.GOOGLE_CLIENT_SECRET || "",
        "scope": ["profile", "email"],
        "callback": "/login/google"
    },
}));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

import { UserService } from "./services/UserService";
import { UserController } from "./controllers/UserController";
const userService = new UserService(knex);
export const userController = new UserController(userService);

import { userRoutes } from "./userRoutes";
app.use("/", userRoutes);

import { MarkdownService } from './services/MarkdownService';
import { MarkdownController } from './controllers/MarkdownController';
const markdownService = new MarkdownService(knex);
export const markdownController = new MarkdownController(markdownService);

import { markdownRoutes } from "./markdownRoutes";
app.use("/", markdownRoutes);

app.use(express.static("public"));

app.use(isLoggedIn, express.static("protected"));

app.listen(PORT, () => {
    console.log(`Listening at http://localhost:${PORT}/`);
});