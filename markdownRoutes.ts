import express from "express";
import { markdownController } from "./app";

export const markdownRoutes = express.Router();

markdownRoutes.get("/markdowns", markdownController.getMarkdowns);
markdownRoutes.get("/markdown/:id", markdownController.getMarkdown);
markdownRoutes.post("/markdown", markdownController.postMarkdown);
markdownRoutes.put("/markdown/is_pinned/:id/:state", markdownController.putPin);
markdownRoutes.put("/markdown/:id", markdownController.putMarkdown);
markdownRoutes.delete("/markdown/:id", markdownController.deleteMarkdown);
markdownRoutes.get("/markdowns/search", markdownController.getSearch);
markdownRoutes.get("/markdowns/with_tag/:tag", markdownController.getMarkdownWithTag);
markdownRoutes.get("/tags", markdownController.getTags);
markdownRoutes.post("/markdown/share", markdownController.shareMarkdown);
markdownRoutes.get("/notifications", markdownController.getNotifications);