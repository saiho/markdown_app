const scroll = new SmoothScroll('a[href*="#"]', {
    speed: 500,
    easing: 'easeOutQuad'
});

// Handle log in
document.querySelector("#login-btn").addEventListener("click", async function (event) {
    event.preventDefault();

    const loginForm = document.querySelector("#login-form");

    // Check the form here. If the form is not valid, stop propagate, which means
    // stop submitting the form, display the invalid feedback.
    // if (loginForm.checkValidity() === false) {
    //     event.preventDefault();
    //     event.stopPropagation();
    //     loginForm.classList.add('was-validated');
    //     return;
    // }

    const response = await fetch('/login', {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            username: loginForm.elements.username1.value,
            password: loginForm.elements.password1.value
        })
    });

    const result = await response.json();

    if (response.status === 200 && result.success) {
        window.location = '/home.html';
    }
    else {
        document.querySelector(".login-response").innerHTML = "Email or password is incorrect!";
    }
});

// Handle sign up
document.querySelector("#signup-btn").addEventListener("click", async function (event) {
    event.preventDefault();

    const signupForm = document.querySelector("#signup-form");

    // Check the form here. If the form is not valid, stop propagate, which means
    // stop submitting the form, display the invalid feedback.
    // if (loginForm.checkValidity() === false) {
    //     event.preventDefault();
    //     event.stopPropagation();
    //     loginForm.classList.add('was-validated');
    //     return;
    // }

    const response = await fetch('/signup', {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            firstname: signupForm.elements.firstname.value,
            lastname: signupForm.elements.lastname.value,
            username: signupForm.elements.username2.value,
            password: signupForm.elements.password2.value
        })
    });

    const result = await response.json();


    if (response.status === 200) {
        $("#signup-modal").modal('hide');
    } else {
        document.querySelector(".signup-response").innerHTML = result.message;
    }
});

document.querySelector(".get-start").addEventListener("click", () => {
    document.querySelector(".signup-btn").click();
});

async function checkLogin() {

    const res = await fetch('/current_user');
    const user = await res.json();
    if (res.status === 200 && user) {
        window.location = '/home.html';
    }
}

checkLogin();

