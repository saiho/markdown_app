import Knex from "knex";
import configs from "../knexfile";
import { UserService } from "./UserService";
const knex = Knex(configs.test);

describe("UserService", () => {
    let userService = new UserService(knex);

    beforeEach(async () => {
        await knex("users").del();
    });

    it("should get Apple", async () => {
        await knex("users")
            .insert({
                firstname: "Apple",
                lastname: "Daily",
                username: "apple@apple.com",
            });
        const user = await userService.foundUser("apple@apple.com");
        expect(user.firstname).toEqual("Apple");
    });

    it("should get apple@apple.com", async () => {
        const user = {
            firstname: "Apple",
            lastname: "Daily",
            username: "apple@apple.com",
            password: "1234",
            profileImg: "",
        }
        await userService.register(user);
        const users = await knex("users").select("*");
        console.log(users.length);
        expect(users.length).toEqual(1);
        expect(users[0].username).toEqual("apple@apple.com")
    });

    afterAll(async () => {
        await knex("users").del();
        knex.destroy();
    });
});