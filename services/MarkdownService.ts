import Knex from "knex";


export class MarkdownService {

    constructor(private knex: Knex) { }

    async getMarkdowns(userId: number) {
        const markdowns = await this.knex.select("*")
            .from("markdowns")
            .where("owner_id", "=", userId)
            .orderBy("is_pinned", 'desc')
            .orderBy("updated_at", 'desc');

        return markdowns;
    }

    async getMarkdown(id: number) {
        const markdown = await this.knex.select("*")
            .from("markdowns")
            .where("id", "=", id)

        return markdown;
    }

    async postMarkdown(ownerId: number) {
        const id = await this.knex("markdowns")
            .insert({ owner_id: ownerId, content: "#new_markdown", is_pinned: false })
            .returning("id");

        await this.knex("tags").insert({ md_id: id[0], name: "#new_markdown", is_active: true });

        return id;
    }

    async putMarkdown(id: number, content: string) {
        await this.knex("tags")
            .update("is_active", false)
            .where("md_id", "=", id);

        const currentTags = await this.knex('tags')
            .select("id", "name")
            .where("md_id", "=", id);

        const regex = /#\w+/g;
        const founds = content.match(regex);

        if (founds) {
            if (currentTags.length < founds.length) {
                for (let i = 0; i < founds.length; i++) {
                    if (i < currentTags.length) {
                        await this.knex("tags")
                            .update({ name: founds[i], is_active: true })
                            .where("id", "=", currentTags[i].id);
                    } else {
                        await this.knex("tags").insert({ md_id: id, name: founds[i], is_active: true });
                    }
                }
            } else {
                for (let i = 0; i < founds.length; i++) {
                    await this.knex("tags")
                        .update({ name: founds[i], is_active: true })
                        .where("id", "=", currentTags[i].id);
                }
            }
        }

        await this.knex("markdowns")
            .update("content", content)
            .update("updated_at", "NOW()")
            .where("id", "=", id);
    }

    async putPin(id: number, state: boolean) {
        await this.knex("markdowns")
            .update("is_pinned", state)
            .where("id", "=", id);
    }

    async deleteMarkdown(id: number) {
        await this.knex("tags")
            .del()
            .where("md_id", "=", id);

        await this.knex("notifications")
            .del()
            .where("md_id", "=", id);

        await this.knex("markdowns")
            .del()
            .where("id", "=", id);
    }

    async getSearch(ownerId: number, query: string) {
        const searchKeys = query.toString().split(" ");

        const markdowns = await this.knex.select("*")
            .from("markdowns")
            .where("owner_id", "=", ownerId)
            .orderBy("is_pinned", 'desc')
            .orderBy("updated_at", 'desc');

        let founds: Set<number> = new Set();

        for (let markdown of markdowns) {
            const content = markdown.content.toUpperCase();

            for (let key of searchKeys) {
                if (content.includes(key.toUpperCase()))
                    founds.add(markdown.id);
            }
        }

        const foundArray = Array.from(founds);

        const searchResults = await this.knex.select("*")
            .from("markdowns")
            .where("id", "in", foundArray)
            .orderBy("is_pinned", 'desc')
            .orderBy("updated_at", 'desc');

        return searchResults;
    }

    async getMarkdownWithTag(ownerId: number, tag: string) {
        const markdowns = await this.knex.select("markdowns.id", "markdowns.owner_id", "markdowns.content", "markdowns.is_pinned", "markdowns.updated_at")
            .from("markdowns")
            .innerJoin("tags", function () {
                this.on("markdowns.id", "=", "tags.md_id")
            })
            .where("owner_id", "=", ownerId)
            .andWhere("tags.name", "=", tag)
            .andWhere("tags.is_active", "=", true)
            .orderBy("is_pinned", 'desc')
            .orderBy("updated_at", 'desc');

        return markdowns;
    }

    async getTags(ownerId: number) {
        const tags = await this.knex.select("tags.name")
            .from("markdowns")
            .innerJoin("tags", function () {
                this.on("tags.md_id", "=", "markdowns.id")
            })
            .where("markdowns.owner_id", "=", ownerId)
            .andWhere("is_active", "=", true)
            .groupBy("tags.name");

        return tags;
    }

    async shareMarkdown(id: number, recipientId: number, ownerId: number) {
        const sharedMarkdown = (await this.knex.select("content")
            .from("markdowns")
            .where("id", "=", id))[0];

        const sharedContent = sharedMarkdown.content + "\n\n#Share";

        const newId = (await this.knex("markdowns")
            .insert({ owner_id: recipientId, content: sharedContent, is_pinned: false })
            .returning("id"))[0];

        await this.knex("notifications")
            .insert({ md_id: id, sender_id: ownerId, reciever_id: recipientId, is_send: false });

        const regex = /#\w+/g;
        const founds = sharedContent.match(regex);

        if (founds) {
            for (let found of founds) {
                await this.knex("tags").insert({ md_id: newId, name: found, is_active: true });
            }
        }
    }

    async getNotifications(recieverId: number) {
        const notifications = await this.knex.select("notifications.id", "users.firstname")
            .from("notifications")
            .innerJoin('users', 'users.id', '=', 'notifications.sender_id')
            .where("reciever_id", "=", recieverId)
            .andWhere("is_send", "=", "false");

        for (let notification of notifications) {
            await this.knex("notifications")
                .update("is_send", true)
                .where("id", "=", notification.id);
        }
        return notifications;
    }
}