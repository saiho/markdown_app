import * as Knex from "knex";
import { hashPassword } from "../hash";

type User = {
    firstname: string,
    lastname: string,
    username: string,
    password: string,
    profileImg: string,
}

export class UserService {
    constructor(private knex: Knex) { }

    async foundUser(username: string) {
        return (await this.knex("users").select("*").where("username", username))[0];
    }

    async register(user: User) {
        const hashedPassword = await hashPassword(user.password);

        await this.knex("users").insert({
            firstname: user.firstname,
            lastname: user.lastname,
            username: user.username,
            password: hashedPassword,
            profile_img: user.profileImg,
        });
    }
}