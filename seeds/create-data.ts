import * as Knex from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("users").del();
    await knex("tags").del();
    await knex("notifications").del();
    await knex("markdowns").del();

    // Inserts seed entries
    await knex("users").insert([
        { firstname: "Alex", lastname: "Lau", username: "alex@tecky.io", password: "$2a$10$T8X8Ox/514xr9VvUeB7i9.ytoTTbNT5Vd/b6Zsl.4HHPxCqJg1Zgu" },
        { firstname: "Jason", lastname: "Li", username: "jason@tecky.io", password: "$2a$10$T8X8Ox/514xr9VvUeB7i9.ytoTTbNT5Vd/b6Zsl.4HHPxCqJg1Zgu" },
        { firstname: "Gordon", lastname: "Lau", username: "gordon@tecky.io", password: "$2a$10$T8X8Ox/514xr9VvUeB7i9.ytoTTbNT5Vd/b6Zsl.4HHPxCqJg1Zgu" },
        { firstname: "Saiho", lastname: "Ip", username: "saiho@gmail.com", password: "$2a$10$T8X8Ox/514xr9VvUeB7i9.ytoTTbNT5Vd/b6Zsl.4HHPxCqJg1Zgu" }
    ]);
};
