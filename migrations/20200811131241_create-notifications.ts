import * as Knex from "knex";

export async function up(knex: Knex) {

    const hasTable = await knex.schema.hasTable('notifications');

    if (hasTable) {
        return Promise.resolve();
    }

    return knex.schema.createTable("notifications", (table) => {
        table.increments();
        table.integer("md_id").references("markdowns.id");
        table.integer("sender_id").references("users.id");
        table.integer("reciever_id").references("users.id");
        table.boolean("is_send");
        table.string("title");
        table.timestamps(true, true);
    });
};

export async function down(knex: Knex) {
    return knex.schema.dropTableIfExists('notifications');
};
