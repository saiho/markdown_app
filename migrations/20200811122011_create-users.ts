import * as Knex from "knex";


export async function up(knex: Knex) {

    const hasTable = await knex.schema.hasTable('users');

    if (hasTable) {
        return Promise.resolve();
    }

    return knex.schema.createTable('users', (table) => {
        table.increments();
        table.string("firstname");
        table.string("lastname");
        table.string("username").unique();
        table.string("password");
        table.string("profile_img");
        table.timestamps(true, true);

    });
};

export async function down(knex: Knex) {
    return knex.schema.dropTableIfExists('users');
};