import * as Knex from "knex";

export async function up(knex: Knex) {

    const hasTable = await knex.schema.hasTable('tags');

    if (hasTable) {
        return Promise.resolve();
    }

    return knex.schema.createTable('tags', (table) => {
        table.increments();
        table.integer("md_id").references('markdowns.id');
        table.string("name")
        table.boolean("is_active");
    });
};

export async function down(knex: Knex) {
    return knex.schema.dropTableIfExists('tags');
};
