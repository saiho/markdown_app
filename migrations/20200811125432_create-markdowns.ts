import * as Knex from "knex";

export async function up(knex: Knex) {

    const hasTable = await knex.schema.hasTable('markdowns');

    if (hasTable) {
        return Promise.resolve();
    }

    return knex.schema.createTable('markdowns', (table) => {
        table.increments();
        table.integer("owner_id").references('users.id');
        table.text("content");
        table.boolean("is_pinned");
        table.timestamps(true, true);
    });
};

export async function down(knex: Knex) {
    return knex.schema.dropTableIfExists('markdowns');
};
