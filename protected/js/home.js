const URL = "https://teachablemachine.withgoogle.com/models/8ixOCioJW/";

async function createModel() {
    const checkpointURL = URL + "model.json"; // model topology
    const metadataURL = URL + "metadata.json"; // model metadata

    const recognizer = speechCommands.create(
        "BROWSER_FFT", // fourier transform type, not useful to change
        undefined, // speech commands vocabulary feature, not useful for your models
        checkpointURL,
        metadataURL);

    // check that model and metadata are loaded via HTTPS requests.
    await recognizer.ensureModelLoaded();

    return recognizer;
}

showdown.setFlavor('github');
const converter = new showdown.Converter({
    'tables': true,
    'strikethrough': true,
    'tasklists': true,
    'ghCodeBlocks': true,
    'parseImgDimensions': true,
});

toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": true,
    "progressBar": true,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "600",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}

const pinIcon = `<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-bookmark" fill="currentColor"
xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd"
    d="M8 12l5 3V3a2 2 0 0 0-2-2H5a2 2 0 0 0-2 2v12l5-3zm-4 1.234l4-2.4 4 2.4V3a1 1 0 0 0-1-1H5a1 1 0 0 0-1 1v10.234z" />
</svg>`
const editor = document.querySelector(".editor");
const preview = document.querySelector(".preview");
const trashBtn = document.querySelector(".trash-btn");
const deleteBtn = document.querySelector(".delete-btn");
const pinBtn = document.querySelector(".pin-btn");
const burger = document.querySelector(".burger");
const navLinks = document.querySelector(".nav-links");
const selections = document.querySelector(".selections");
const backSelections = document.querySelector(".back-to-selections");
const backPreview = document.querySelector(".back-to-preview");
const newBtn = document.querySelector(".new-btn");
const previewPanel = document.querySelector(".preview-wrapper");
const editorPanel = document.querySelector(".editor-wrapper");
const editBtn = document.querySelector(".edit-btn");
const shareBtn = document.querySelector(".share-btn");
const confirmShareBtn = document.querySelector("#share-confirm");
let currentTag = "All";
let currentSelected;
const pages = { SELECTIONS: 1, PREVIEW: 2, EDIT: 3 };
let page = pages.SELECTIONS;
const classes = { BACK: 1, BOOKMARK: 2, DELETE: 3, DOWN: 4, EDIT: 5, NEW: 6, SHARE: 7, UP: 8 };

shareBtn.addEventListener("click", () => {
    document.querySelector("#found").innerHTML = "";
    document.querySelector("#not-found").innerHTML = "";
    document.querySelector("#recipient-name").value = "";
})

confirmShareBtn.addEventListener("click", async () => {
    const recipient = document.querySelector("#recipient-name").value;
    const markdownId = parseInt(editor.dataset.id);

    const resposne1 = await fetch(`/user/${recipient}`)
    const result1 = await resposne1.json();

    if (resposne1.status === 404) {
        document.querySelector("#found").innerHTML = "";
        document.querySelector("#not-found").innerHTML = result1.message;
        return;
    }

    const recipientId = result1;

    const response2 = await fetch(`/markdown/share`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            markdownId: markdownId,
            recipientId: recipientId
        })
    })

    const result2 = await response2.json();

    if (response2.status === 200) {
        document.querySelector("#not-found").innerHTML = "";
        document.querySelector("#found").innerHTML = result2.message;
    }
})

pinBtn.addEventListener("click", async () => {
    const hasId = event.currentTarget.parentNode.previousElementSibling.dataset.id;

    if (!hasId)
        return;

    event.currentTarget.classList.toggle("active");
    const state = event.currentTarget.classList.contains("active");

    await fetch(`/markdown/is_pinned/${hasId}/${state}`, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json"
        }
    })

    main();
});

editor.addEventListener("input", async () => {
    preview.innerHTML = converter.makeHtml(editor.value);

    const id = event.currentTarget.dataset.id;

    await fetch(`/markdown/${id}`, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            content: editor.value
        })
    })

    if (currentTag == "All") {
        main();
        return;
    }

    const response = await fetch(`/markdowns/with_tag/${currentTag.substring(1)}`);
    const markdowns = await response.json();

    loadSelections(markdowns);
    addListenerToSelections();
    loadBurgerMenu();
})

editor.addEventListener("onchange", () => {
    preview.innerHTML = converter.makeHtml(editor.value);
})

burger.addEventListener("click", () => {
    toggleBurgerMenu();
});

newBtn.addEventListener("click", async () => {
    const response = await fetch("/markdown", { method: "POST", });
    main();
});

loadSelections = (markdowns) => {
    selections.innerHTML = "";

    for (let markdown of markdowns) {
        const content = markdown.content;
        const pin = markdown.is_pinned ? pinIcon : "";
        let active = "";

        if (currentSelected && currentSelected == markdown.id) {
            active = " active";
        }

        const lastUpdated = new Date(markdown.updated_at);

        selections.innerHTML +=
            `<div class="selection${active}" data-id="${markdown.id}">
                <div class="selection-content${active}">
                    <div class="selection-left">
                        <div class="updated-at">
                            ${getTimeElapsed(lastUpdated)}
                        </div>
                        <div class="pin">
                            ${pin}
                        </div>
                    </div>
                    <div class="selection-right">
                        ${convertHeader(converter.makeHtml(content))}
                    </div>
                </div>
            </div>`;
    }
}

convertHeader = (content) => {
    let newContent = content;
    newContent = newContent.replace("h1", "h6");
    newContent = newContent.replace("h2", "h6");
    newContent = newContent.replace("h3", "h6");
    newContent = newContent.replace("h4", "h6");
    newContent = newContent.replace("h5", "h6");

    return newContent;
}

getTimeElapsed = (lastUpdated) => {

    const now = new Date();
    const elapsed = (now - lastUpdated) / 1000;

    if (Math.floor(elapsed) == 0) {
        return "0s"
    }
    else if (Math.floor(elapsed / 60) < 1) {
        return Math.floor(elapsed) + "s";
    }
    else if (Math.floor(elapsed / (60 * 60) < 1)) {
        return Math.floor(elapsed / 60) + "m";
    }
    else if (Math.floor(elapsed / (60 * 60 * 24) < 1)) {
        return Math.floor(elapsed / (60 * 60)) + "h";
    }
    else if (Math.floor(elapsed / (60 * 60 * 24 * 30) < 1)) {
        return Math.floor(elapsed / (60 * 60 * 24)) + "D";
    }
    else if (Math.floor(elapsed / (60 * 60 * 24 * 30 * 12) < 1)) {
        return Math.floor(elapsed / (60 * 60 * 24 * 30)) + "M";
    }
    else {
        return Math.floor(elapsed / (60 * 60 * 24 * 30 * 12)) + "Y";
    }
}

addListenerToSelections = async () => {
    const allSelections = document.querySelectorAll(".selection");

    for (let selection of allSelections) {
        selection.addEventListener("click", async () => {
            document.querySelector(".btn-group").classList.remove("d-none");

            const selects = document.querySelectorAll(".selection");
            for (let select of selects) {
                select.classList.remove("active");
                select.children[0].classList.remove("active");
            }

            event.currentTarget.classList.add("active");
            event.currentTarget.children[0].classList.add("active");

            const id = event.currentTarget.dataset.id;
            currentSelected = id;
            const response = await fetch(`/markdown/${id}`);
            const markdown = (await response.json())[0];

            editor.readOnly = false;
            editor.dataset.id = markdown.id;
            editor.value = markdown.content;
            preview.innerHTML = converter.makeHtml(markdown.content);

            markdown.is_pinned ? pinBtn.classList.add("active") : pinBtn.classList.remove("active");

            if (window.innerWidth <= 800) {
                burger.classList.add("d-none");
                backSelections.classList.remove("d-none");
                previewPanel.style.left = 0;
                page = pages.PREVIEW;
            }
        })
    }
}

backSelections.addEventListener("click", () => {
    previewPanel.style.left = "100%";
    backSelections.classList.add("d-none");
    backPreview.classList.add("d-none");
    burger.classList.remove("d-none");
    page = pages.SELECTIONS;
});

backPreview.addEventListener("click", () => {
    editorPanel.style.left = "100%";
    backPreview.classList.add("d-none");
    burger.classList.add("d-none");
    backSelections.classList.remove("d-none");
    page = pages.PREVIEW;
});

editBtn.addEventListener("click", () => {
    editorPanel.style.left = 0;
    backSelections.classList.add("d-none");
    burger.classList.add("d-none");
    backPreview.classList.remove("d-none");
    page = pages.EDIT;
})

loadBurgerMenu = async () => {
    const response = await fetch("/tags");
    const tags = await response.json();

    navLinks.innerHTML = `<li>All</li>`

    for (let tag of tags)
        navLinks.innerHTML += `<li>${tag.name}</li>`

    navLinks.innerHTML += `<li class="logout">Logout</li>`

    for (let li of navLinks.children) {
        li.addEventListener("click", async () => {
            currentTag = li.innerHTML;

            if (currentTag == "Logout") {
                const response = await fetch("/logout");
                const result = await response.json();

                if (response.status === 200) {
                    window.location = '/';
                } else {
                    console.log(result);
                }
            }

            if (currentTag == "All") {
                toggleBurgerMenu();
                main();
                return;
            }

            const response = await fetch(`/markdowns/with_tag/${currentTag.substring(1)}`);
            const markdowns = await response.json();

            loadSelections(markdowns);
            addListenerToSelections();
            restEditorAndPreiew();
            toggleBurgerMenu();
        })
    }
}

loadNotifications = async () => {
    const response = await fetch("/notifications");
    const notifications = await response.json();

    if (notifications[0]) {
        for (let notification of notifications) {
            toastr["info"](`${notification.firstname} sent you a new markdown`, `You got a new markdown`);
        }
    }
}

loadProfile = async () => {
    const response = await fetch("/current_user");
    const user = await response.json();

    const profileIcon = document.querySelector(".profile-icon");
    const firstname = document.querySelector(".first-name");

    firstname.innerHTML = user.firstname;

    if (user.profile_img) {
        profileIcon.style.backgroundImage = `url("${user.profile_img}")`;
        profileIcon.style.backgroundSize = "40px 40px";
    }
    else {
        const userIntial = user.firstname[0] + user.lastname[0];
        profileIcon.innerHTML = userIntial.toUpperCase();
    }
}

loadProfile();

deleteBtn.addEventListener("click", async () => {
    // const hasId = event.currentTarget.parentNode.previousElementSibling.dataset.id;
    const hasId = document.querySelector(".editor").dataset.id;

    if (!hasId)
        return;

    await fetch(`/markdown/${hasId}`, {
        method: "DELETE"
    });

    if (window.innerWidth <= 800) {
        backPreview.click();
        backSelections.click();
    }

    restEditorAndPreiew();
    main();
    $('#delete-confirmation').modal('hide')
});

document.querySelector(".search").addEventListener("input", async () => {
    const query = event.currentTarget.value;

    if (query.trim() === "")
        return;

    const response = await fetch(`/markdowns/search?query=${query}`)
    const markdowns = await response.json();

    loadSelections(markdowns);
    addListenerToSelections();
    restEditorAndPreiew();
});

document.querySelector(".clear-btn").addEventListener("click", async () => {
    document.querySelector(".search").value = "";
    main();
});

toggleBurgerMenu = () => {
    burger.classList.toggle("toggle");
    navLinks.classList.toggle("toggle");
}

restEditorAndPreiew = () => {
    editor.value = "";
    editor.readOnly = true;
    preview.innerHTML = "";
    pinBtn.classList.remove("active");
}

window.addEventListener("resize", async () => {
    if (window.innerWidth > 800) {
        burger.classList.remove("d-none");
        backSelections.classList.add("d-none");
        backPreview.classList.add("d-none");
        page = pages.SELECTIONS;
        previewPanel.style.left = 0;
        editorPanel.style.left = 0;
    }
    if (window.innerWidth <= 800) {
        if (page == pages.SELECTIONS) {
            previewPanel.style.left = "100%";
            editorPanel.style.left = "100%";
        }
    }
});

initVoiceRecognizer = async () => {

    const recognizer = await createModel();
    console.log("creating model...")
    const classLabels = recognizer.wordLabels();

    recognizer.listen(result => {
        const scores = result.scores; // probability of prediction for each class
        console.log("background: " + scores[0].toFixed(2));

        if (window.innerWidth <= 800) {
            if (scores[classes.BACK].toFixed(2) >= 0.75) {
                if (page == pages.PREVIEW) {
                    console.log("back: " + scores[classes.BACK].toFixed(2));
                    backSelections.click();
                    console.log("Back to Selections");
                } else if (page == pages.EDIT) {
                    backPreview.click();
                    console.log("Back to Preview");
                }
            } else if (scores[classes.UP].toFixed(2) >= 0.75) {
                if (page == pages.SELECTIONS) {
                    console.log("up: " + scores[classes.UP].toFixed(2));
                    selections.scrollBy({
                        top: -300,
                        behavior: 'smooth'
                    });
                } else if (page == pages.PREVIEW) {
                    console.log("up: " + scores[classes.UP].toFixed(2));
                    preview.scrollBy({
                        top: -300,
                        behavior: 'smooth'
                    });
                } else if (page == pages.EDIT) {
                    console.log("up: " + scores[classes.UP].toFixed(2));
                    editor.scrollBy({
                        top: -300,
                        behavior: 'smooth'
                    });
                }
            } else if (scores[classes.DOWN].toFixed(2) >= 0.75) {
                if (page == pages.SELECTIONS) {
                    console.log("down: " + scores[classes.DOWN].toFixed(2));
                    selections.scrollBy({
                        top: 300,
                        behavior: 'smooth'
                    });
                } else if (page == pages.PREVIEW) {
                    console.log("down: " + scores[classes.DOWN].toFixed(2));
                    preview.scrollBy({
                        top: 300,
                        behavior: 'smooth'
                    });
                } else if (page == pages.EDIT) {
                    console.log("down: " + scores[classes.DOWN].toFixed(2));
                    editor.scrollBy({
                        top: 300,
                        behavior: 'smooth'
                    });
                }
            } else if (scores[classes.DELETE].toFixed(2) >= 0.75) {
                if (page == pages.EDIT) {
                    console.log("delete: " + scores[classes.DELETE].toFixed(2));
                    trashBtn.click();
                    backPreview.click();
                    backSelections.click();
                }
            } else if (scores[classes.SHARE].toFixed(2) >= 0.75) {
                if (page == pages.EDIT) {
                    console.log("share: " + scores[classes.SHARE].toFixed(2));
                    shareBtn.click();
                }
            } else if (scores[classes.NEW].toFixed(2) >= 0.75) {
                if (page == pages.SELECTIONS) {
                    newBtn.click();
                    console.log(page);
                    console.log("new: " + scores[classes.NEW].toFixed(2));
                } else if (page == pages.PREVIEW) {
                    newBtn.click();
                    console.log("new: " + scores[classes.NEW].toFixed(2));
                    console.log(page);
                    backSelections.click();
                    page == pages.SELECTIONS;
                } else if (page == pages.EDIT) {
                    newBtn.click();
                    console.log("new: " + scores[classes.NEW].toFixed(2));
                    console.log(page);
                    backPreview.click();
                    backSelections.click();
                    page == pages.SELECTIONS;
                }
            } else if (scores[classes.EDIT].toFixed(2) >= 0.75) {
                if (page == pages.PREVIEW) {
                    editBtn.click();
                    console.log("edit: " + scores[classes.EDIT].toFixed(2));
                    console.log(page);
                }
            } else if (scores[classes.BOOKMARK].toFixed(2) >= 0.75) {
                if (page == pages.EDIT) {
                    pinBtn.click();
                    console.log("bookmark: " + scores[classes.BOOKMARK].toFixed(2));
                    console.log(page);
                }
            }
        }
        else {
            if (scores[classes.UP].toFixed(2) >= 0.75) {
                console.log("up: " + scores[classes.UP].toFixed(2));
                selections.scrollBy({
                    top: -300,
                    behavior: 'smooth'
                });
                editor.scrollBy({
                    top: -300,
                    behavior: 'smooth'
                });
                preview.scrollBy({
                    top: -300,
                    behavior: 'smooth'
                });
            } else if (scores[classes.DOWN].toFixed(2) >= 0.75) {
                console.log("down: " + scores[classes.DOWN].toFixed(2));
                selections.scrollBy({
                    top: 300,
                    behavior: 'smooth'
                });
                editor.scrollBy({
                    top: 300,
                    behavior: 'smooth'
                });
                preview.scrollBy({
                    top: 300,
                    behavior: 'smooth'
                });
            } else if (scores[classes.DELETE].toFixed(2) >= 0.75) {
                console.log("delete: " + scores[classes.DELETE].toFixed(2));
                trashBtn.click();
            } else if (scores[classes.SHARE].toFixed(2) >= 0.75) {
                console.log("share: " + scores[classes.SHARE].toFixed(2));
                shareBtn.click();
            } else if (scores[classes.NEW].toFixed(2) >= 0.75) {
                console.log("new: " + scores[classes.NEW].toFixed(2));
                newBtn.click();
            } else if (scores[classes.BOOKMARK].toFixed(2) >= 0.75) {
                pinBtn.click();
                console.log("bookmark: " + scores[classes.BOOKMARK].toFixed(2));
            }
        }
    }, {
        includeSpectrogram: true, // in case listen should return result.spectrogram
        probabilityThreshold: 0.75,
        invokeCallbackOnNoiseAndUnknown: true,
        overlapFactor: 0.4 // probably want between 0.5 and 0.75. More info in README
    });
}

main = async () => {
    const resposne1 = await fetch("/markdowns");
    const markdowns = await resposne1.json();
    loadNotifications();
    loadSelections(markdowns);
    addListenerToSelections();
    loadBurgerMenu();
}

main();
initVoiceRecognizer();