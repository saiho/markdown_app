import express from 'express';

export const isLoggedIn = function (req: express.Request, res: express.Response, next: express.NextFunction) {

    if (req.session && req.session.user) {
        next();
    } else {
        res.status(401).redirect("/");
    }
}