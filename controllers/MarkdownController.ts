import { MarkdownService } from '../services/MarkdownService';
import { Request, Response } from 'express';


export class MarkdownController {

    constructor(private service: MarkdownService) { }

    getMarkdowns = async (req: Request, res: Response) => {
        try {
            if (req.session && req.session.user) {
                const markdowns = await this.service.getMarkdowns(req.session.user.id);
                res.status(200).json(markdowns);
            } else {
                res.status(401).json({ message: "User not logged in!" })
            }
        } catch (err) {
            console.log(err.message);
            res.status(500).json({ message: "Internal server error!" });
        }
    };

    getMarkdown = async (req: Request, res: Response) => {
        try {
            if (req.session && req.session.user) {
                const id = parseInt(req.params.id)
                const markdown = await this.service.getMarkdown(id);
                res.status(200).json(markdown);
            } else {
                res.status(401).json({ message: "User not logged in!" })
            }
        } catch (err) {
            console.log(err.message);
            res.status(500).json({ message: "Internal server error!" });
        }
    };

    postMarkdown = async (req: Request, res: Response) => {
        try {
            if (req.session && req.session.user) {
                const id = await this.service.postMarkdown(req.session.user.id);
                res.status(200).json(id);
            } else {
                res.status(401).json({ message: "User not logged in!" })
            }
        } catch (err) {
            console.log(err.message);
            res.status(500).json({ message: "Internal server error!" });
        }
    }

    putPin = async (req: Request, res: Response) => {
        try {
            if (req.session && req.session.user) {
                const id = parseInt(req.params.id);
                const state = (req.params.state == "true");
                await this.service.putPin(id, state);

                res.status(200).json({ message: "Successfully updated!" });
            } else {
                res.status(401).json({ message: "User not logged in!" })
            }
        } catch (err) {
            console.log(err.message);
            res.status(500).json({ message: "Internal server error!" });
        }
    }

    putMarkdown = async (req: Request, res: Response) => {
        try {
            if (req.session && req.session.user) {
                const id = parseInt(req.params.id);
                const content = req.body.content;
                await this.service.putMarkdown(id, content);

                res.status(200).json({ message: "Successfully updated!" });
            } else {
                res.status(401).json({ message: "User not logged in!" })
            }
        } catch (err) {
            console.log(err.message);
            res.status(500).json({ message: "Internal server error!" });
        }
    }

    deleteMarkdown = async (req: Request, res: Response) => {
        try {
            if (req.session && req.session.user) {
                const id = parseInt(req.params.id);
                await this.service.deleteMarkdown(id);

                res.status(200).json({ message: "Successfully updated!" });
            } else {
                res.status(401).json({ message: "User not logged in!" })
            }
        } catch (err) {
            console.log(err.message);
            res.status(500).json({ message: "Internal server error!" });
        }
    }

    getSearch = async (req: Request, res: Response) => {
        try {
            if (req.session && req.session.user) {
                const ownerId = req.session.user.id;
                const hasQuery = req.query.query;

                if (!hasQuery) {
                    res.json({ message: "No search query is found!" });
                }

                const markdowns = await this.service.getSearch(ownerId, String(hasQuery));
                res.status(200).json(markdowns);
            } else {
                res.status(401).json({ message: "User not logged in!" })
            }
        } catch (err) {
            console.log(err.message);
            res.status(500).json({ message: "Internal server error!" });
        }
    }

    getMarkdownWithTag = async (req: Request, res: Response) => {
        try {
            if (req.session && req.session.user) {
                const tag = '#' + req.params.tag;
                const ownerId = req.session.user.id;

                const markdowns = await this.service.getMarkdownWithTag(ownerId, tag);
                res.status(200).json(markdowns);
            } else {
                res.status(401).json({ message: "User not logged in!" })
            }
        } catch (err) {
            console.log(err.message);
            res.status(500).json({ message: "Internal server error!" });
        }
    }

    getTags = async (req: Request, res: Response) => {
        try {
            if (req.session && req.session.user) {
                const tags = await this.service.getTags(req.session.user.id);
                res.status(200).json(tags);
            } else {
                res.status(401).json({ message: "User not logged in!" })
            }
        } catch (err) {
            console.log(err.message);
            res.status(500).json({ message: "Internal server error!" });
        }
    }

    shareMarkdown = async (req: Request, res: Response) => {
        try {
            if (req.session && req.session.user) {
                const { markdownId, recipientId } = req.body;
                await this.service.shareMarkdown(markdownId, recipientId, req.session.user.id);

                res.status(200).json({ message: "Markdown has been shared successfully!" });
            } else {
                res.status(401).json({ message: "User not logged in!" })
            }
        } catch (err) {
            console.log(err.message);
            res.status(500).json({ message: "Internal server error!" });
        }
    }

    getNotifications = async (req: Request, res: Response) => {
        try {
            if (req.session && req.session.user) {
                const notifications = await this.service.getNotifications(req.session.user.id);

                res.status(200).json(notifications);
            } else {
                res.status(401).json({ message: "User not logged in!" })
            }
        } catch (err) {
            console.log(err.message);
            res.status(500).json({ message: "Internal server error!" });
        }
    }
}