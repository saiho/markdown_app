import { Request, Response } from 'express';
import { UserService } from '../services/UserService';
import { checkPassword, hashPassword } from '../hash';
import fetch from 'node-fetch';


export class UserController {

    constructor(private service: UserService) { }

    login = async (req: Request, res: Response) => {
        try {
            const found = await this.service.foundUser(req.body.username);

            if (found && await checkPassword(req.body.password, found.password)) {
                if (req.session)
                    req.session.user = found;

                res.status(200).json({ success: true });
            } else {
                res.status(401).json({ success: false });
            }
        } catch (err) {
            console.log(err.message);
            res.status(500).json({ message: "internal server error!" });
        }
    }

    logout = (req: Request, res: Response) => {
        try {
            if (req.session && req.session.user) {
                req.session.user = null;
                res.status(200).json({ sucess: true });
            } else {
                res.status(400).json({ sucess: false });
            }
        } catch (err) {
            console.log(err.message);
            res.status(500).json({ message: "internal server error!" });
        }
    }

    currentUser = (req: Request, res: Response) => {
        try {
            if (req.session && req.session.user) {
                const user = req.session.user;
                user.password = null
                res.status(200).json(user);
            } else {
                res.status(401).json({ message: "User is not logged in" });
            }
        } catch (err) {
            console.log(err.message);
            res.status(500).json({ message: "internal server error!" });
        }
    }

    getId = async (req: Request, res: Response) => {
        try {
            if (req.session && req.session.user) {
                const username = req.params.username
                const found = await this.service.foundUser(username);

                if (!found)
                    res.status(404).json({ message: "User was not found!" });

                res.status(200).json(found.id);
            } else {
                res.status(401).json({ message: "User is not logged in" });
            }
        } catch (err) {
            console.log(err.message);
            res.status(500).json({ message: "internal server error!" });
        }
    }

    signup = async (req: Request, res: Response) => {
        const user = req.body;
        try {
            const found = await this.service.foundUser(user.username);

            if (found) {
                res.status(401).json({ message: "This email address has been registered before." });
            } else {
                await this.service.register(user);
                res.status(200).json({ message: "A new user has been registered" });
            }
        } catch (err) {
            console.log(err.message);
            res.status(500).json({ message: "internal server error!" });
        }
    }

    loginGoogle = async (req: Request, res: Response) => {
        const accessToken = req.session?.grant.response.access_token;
        const googleResponse = await fetch('https://www.googleapis.com/oauth2/v2/userinfo', {
            headers: {
                "Authorization": `Bearer ${accessToken}`
            }
        })
        const googleUser = await googleResponse.json();
        let found = await this.service.foundUser(googleUser.email);

        if (req.session) {
            if (found) {
                req.session.user = found;
            } else {
                const password = await hashPassword(Math.random().toString(36).substring(2));

                await this.service.register({
                    firstname: googleUser.given_name,
                    lastname: googleUser.family_name,
                    username: googleUser.email,
                    password: password,
                    profileImg: googleUser.picture,
                })

                found = await this.service.foundUser(googleUser.email);
                req.session.user = found;
            }
        }
        res.redirect('/home.html');
    }
}