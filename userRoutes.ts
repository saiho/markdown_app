import express from "express";
import { userController } from "./app";

export const userRoutes = express.Router();

userRoutes.post("/login", userController.login);
userRoutes.get("/logout", userController.logout);
userRoutes.get('/current_user', userController.currentUser);
userRoutes.get('/user/:username', userController.getId);
userRoutes.post('/signup', userController.signup);
userRoutes.get('/login/google', userController.loginGoogle);
